<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// workshop 2

// Route::apiResource('students', 'StudentController');





// Route::apiResource('students', function(){

// })->middleware('auth.base');



// Workshop 3

Route::apiResource('students', 'StudentController')->middleware('auth.basic');


// Route::get('students', 'StudentController@index')->middleware('auth.basic');

// Route::post('students', 'StudentController@store')->middleware('auth.basic');

// Route::PUT('students/{id}', 'StudentController@update',function ($request,$id){ 
//     return 'productos'.$id;
//   });

//   Route::delete('students/{id}', 'StudentController@destroy');
